<?php get_header(); ?>
<div id="main-contento">
	<div class="containero">
		<div id="content-area" class="clearfix">
			<div class="et_pb_extra_column_main">
				<?php
				do_action( 'et_before_post' );

				if ( have_posts() ) :
					while ( have_posts() ) : the_post(); ?>
						<?php
							$post_category_color = extra_get_post_category_color();
						?>
						<article id="post-<?php the_ID(); ?>" <?php post_class( 'module single-post-module' ); ?>>


							<?php if ( ( et_has_post_format() && et_has_format_content() ) || ( has_post_thumbnail() && is_post_extra_featured_image_enabled() ) ) { ?>
							<div class="post-thumbnail header">
								<?php
								$score_bar = extra_get_the_post_score_bar();
								$thumb_args = array( 'size' => 'extra-image-single-post', 'link_wrapped' => false );
								require locate_template( 'post-top-content.php' );
								?>
							</div>
							<?php } ?>

                            <?php if ( is_post_extra_title_meta_enabled() ) { ?>
							<div class="post-header">
								<h1 class="entry-title"><?php the_title(); ?></h1>
								<div class="post-meta vcard">
									<p><?php echo extra_display_single_post_meta(); ?></p>
								</div>
							</div>
							<?php } ?>

							<?php $post_above_ad = extra_display_ad( 'post_above', false ); ?>
							<?php if ( !empty( $post_above_ad ) ) { ?>
							<div class="et_pb_extra_row etad post_above">
								<?php echo $post_above_ad; ?>
							</div>
							<?php } ?>

							<?php

                                /*$post_ID = get_the_ID();
                                $room_bathroom = get_field("room_bathroom", $post_ID);
                                $room_as_single = get_field("room_as_single", $post_ID);
								$icon_path = get_stylesheet_directory_uri()."/svg-icons/rooms/";
								$beds_large = get_field("room_beds_large", $post_ID);
								$beds_small = get_field("room_beds_small", $post_ID);
								$icon_bed_large = file_get_contents($icon_path."large-bed.svg");
								$icon_bed_small = file_get_contents($icon_path."small-bed.svg");
                                $post_category = get_the_category( $post_ID );
                                $post_category = $post_category[0]->name;*/

                                $post_ID = get_the_ID();

                                $offer_price = get_field("prezzo_offerta", $post_ID);
                                $offer_price_startfrom = get_field("prezzo_offerta_start", $post_ID);
                                $price_for = get_field("specifica_prezzo", $post_ID);

                                $offer_price_2 = get_field("prezzo_offerta_secondo", $post_ID);
                                $offer_price_startfrom_2 = get_field("prezzo_offerta_start_secondo", $post_ID);
                                $price_for_2 = get_field("specifica_prezzo_secondo", $post_ID);

                                $offer_list = get_field("lista_offerta", $post_ID);
                                $offer_start_row = get_field("inizio_promozione", $post_ID);
								$offer_start = strtotime($offer_start_row);
								$offer_start = date("d/m/Y",$offer_start);
                                $offer_end_row = get_field("fine_promozione", $post_ID);
                                $offer_end = strtotime($offer_end_row);
                                $offer_end = date("d/m/Y",$offer_end);
                                $offer_link = get_field("link_offerta_booking_engine", $post_ID);

                                $post_image_full = get_the_post_thumbnail( $post_ID, 'full' );

                                $offer_list_array = explode(";",$offer_list);
                                $offer_excerpt = get_the_excerpt( $post_ID );

                                $offer_marketing_title = get_field("titolo_commerciale_offerta", $post_ID);
                                $offer_marketing_title = ( $offer_marketing_title != "") ? $offer_marketing_title : get_the_title();

                            ?>

                            <div class="post-content entry-content fc-theme__landingpage">

                                <header class="fc-theme__landingpage_header">
                                    <?php echo $post_image_full; ?>
                                    <div class="fc-theme__landingpage_header_title">

                                        <div class="module_double-title">
                                            <div class="title-background"><?php echo __('Stay Offer','riparelais'); ?></div>
                                            <div class="title-front"><?php echo get_the_title(); ?></div>
                                        </div>

                                    </div>
                                </header>

                                <div class="fc-theme__landingpage_bookingform">
									<div class="container">
										<h4><?php echo __('Verifica Disponibilità'); ?></h4>
										<?php echo do_shortcode( '[bookingform day_start="2018,1,14" day_end="2018,1,28"]' );?>
									</div>
								</div>

                                <div class="fc-theme__landingpage_body">
                                    <div class="fc-theme__landingpage_row container">

                                        <div class="fc-theme__landingpage_col">

                                            <h1><?php echo $offer_marketing_title; ?></h1>

											<?php if(has_excerpt($post_ID)): ?>
                                            <div class="fc-theme__landingpage_excerpt">
                                                <?php echo $offer_excerpt; ?>
                                            </div>
                                            <?php endif; ?>

                                            <?php if($offer_start != "" || $offer_date != ""): ?>
                                            <div class="fc-theme__landingpage_dates">

                                                <?php if($offer_start != ""): ?>
                                                <div class="fc-theme__landingpage_date-start">
                                                    <?php echo "<span>". __('Offerta valida dal')."</span> <span>" . $offer_start . "</span>"; ?>
                                                </div>
                                                <?php endif; ?>

                                                <?php if($offer_end != ""): ?>
                                                <div class="fc-theme__landingpage_date-end">
                                                    <?php echo "<span>". __('al')."</span> <span>" . $offer_end . "</span>"; ?>
                                                </div>
                                                <?php endif; ?>

                                            </div>
                                            <?php endif; ?>

                                            <ul class="fc-theme__landingpage_list">
                                                <?php foreach($offer_list_array as $key => $offer_list_item): ?>
                                                <li><i class=" icon_check"></i> <?php echo $offer_list_item; ?></li>
                                                <?php endforeach; ?>
                                            </ul>

                                            <?php if($offer_price != ""): ?>
                                            <div class="fc-theme__landingpage_price">
                                               <?php if($offer_price_startfrom  == TRUE): ?><span class="price-start-from"><?php echo __('a partire da'); ?></span><?php endif; ?> <span class="price-currency">€</span> <span class="price-amount"><?php echo $offer_price; ?></span> <span class="price-for">(<?php echo $price_for; ?>)</span>
                                            </div>
                                            <?php endif; ?>

                                            <?php if($offer_price_2 != ""): ?>
                                            <div class="fc-theme__landingpage_price">
                                               <?php if($offer_price_startfrom_2  == TRUE): ?><span class="price-start-from"><?php echo __('a partire da'); ?></span><?php endif; ?> <span class="price-currency">€</span> <span class="price-amount"><?php echo $offer_price_2; ?></span> <span class="price-for">(<?php echo $price_for_2; ?>)</span>
                                            </div>
                                            <?php endif; ?>

                                            <?php if($offer_link != ""): ?>
                                                <div class="fc-theme__landingpage_bookbutton">
                                                    <a class="button button-green" href="<?php echo $offer_link; ?>" target="_blank"><?php echo __('prenota subito online'); ?></a>
                                                    <a class="button button-black" href="tel:0756020131"><?php echo __('chiamaci'); ?> 0756020131</a>
                                                </div>
                                            <?php endif; ?>


                                        </div>

                                        <div class="fc-theme__landingpage_col">
                                            <div class="fc-theme__landingpage_form">
                                                <?php echo __('<h4>Vuoi maggiori informazioni su questa offerta?</h4> <p class="fc-theme__landingpage_form-description">Usa il form qui sotto per chiederci tutto quello che vuoi.</p>'); ?>
                                                <?php echo do_shortcode( '[contact-form-7 id="2292" title="Contact Form"]' ); ?>
                                            </div>
                                        </div>

                                    </div>

									<div class="fc-theme__landingpage_bookingform">
										<div class="container">
											<h4><?php echo __('Verifica Disponibilità'); ?></h4>
											<?php echo do_shortcode( '[bookingform day_start="2018,1,14" day_end="2018,1,28"]' );?>
										</div>
									</div>

                                    <div class="post-wrap">
                                        <div class="container fc-theme__landingpage_content">
											<h3><?php echo __('Dettaglio dell\'offerta'); ?></h3>
                                            <?php the_content(); ?>
                                        </div>
                                    </div>
                                </div>


                            </div>


                            <!-- POST AFTER CONTENT -->
							<?php if ( $review = extra_post_review() ) { ?>
							<div class="post-wrap post-wrap-review">
								<div class="review">
									<div class="review-title">
										<h3><?php echo esc_html( $review['title'] ); ?></h3>
									</div>
									<div class="review-content">
										<div class="review-summary clearfix">
											<div class="review-summary-score-box" style="background-color:<?php echo esc_attr( $post_category_color ); ?>">
												<h4><?php printf( et_get_safe_localization( __( '%d%%', 'extra' ) ), absint( $review['score'] ) ); ?></h4>
											</div>
											<div class="review-summary-content">
												<?php if ( !empty( $review['summary'] ) ) { ?>
												<p>
													<?php if ( !empty( $review['summary_title'] ) ) { ?>
														<strong><?php echo esc_html( $review['summary_title'] ); ?></strong>
													<?php } ?>
													<?php echo $review['summary']; ?>
												</p>
												<?php } ?>
											</div>
										</div>
										<div class="review-breakdowns">
											<?php foreach ( $review['breakdowns'] as $breakdown ) { ?>
											<div class="review-breakdown">
												<h5 class="review-breakdown-title"><?php echo esc_html( $breakdown['title'] ); ?></h5>
												<div class="score-bar-bg">
													<span class="score-bar" style="background-color:<?php echo esc_attr( $post_category_color ); ?>; width:<?php printf( '%d%%', max( 4, absint( $breakdown['rating'] ) ) );  ?>">
														<span class="score-text"><?php printf( et_get_safe_localization( __( '%d%%', 'extra' ) ), absint( $breakdown['rating'] ) ); ?></span>
													</span>
												</div>
											</div>
											<?php } ?>
										</div>
									</div>
								</div>
							</div>
							<?php } ?>
							<div class="post-footer">
								<div class="social-icons ed-social-share-icons">
									<p class="share-title"><?php esc_html_e( 'Share:', 'extra' ); ?></p>
									<?php extra_post_share_links(); ?>
								</div>
								<?php if ( extra_is_post_rating_enabled() ) { ?>
								<div class="rating-stars">
									<?php extra_rating_stars_display(); ?>
								</div>
								<?php } ?>
								<style type="text/css" id="rating-stars">
									.post-footer .rating-stars #rated-stars img.star-on,
									.post-footer .rating-stars #rating-stars img.star-on {
										background-color: <?php echo esc_html( $post_category_color ); ?>;
									}
								</style>
							</div>

							<?php $post_below_ad = extra_display_ad( 'post_below', false ); ?>
							<?php if ( !empty( $post_below_ad ) ) { ?>
							<div class="et_pb_extra_row etad post_below">
								<?php echo $post_below_ad; ?>
							</div>
							<?php } ?>
						</article>

                        <!--
						<nav class="post-nav">
							<div class="nav-links clearfix">
								<div class="nav-link nav-link-prev">
									<?php previous_post_link( '%link', et_get_safe_localization( __( '<span class="button">Previous</span><span class="title">%title</span>', 'extra' ) ) ); ?>
								</div>
								<div class="nav-link nav-link-next">
									<?php next_post_link( '%link', et_get_safe_localization( __( '<span class="button">Next</span><span class="title">%title</span>', 'extra' ) ) ); ?>
								</div>
							</div>
						</nav>
                        -->
						<?php
						if ( extra_is_post_author_box() ) { ?>
						<div class="et_extra_other_module author-box vcard">
							<div class="author-box-header">
								<h3><?php esc_html_e( 'About The Author', 'extra' ); ?></h3>
							</div>
							<div class="author-box-content clearfix">
								<div class="author-box-avatar">
									<?php echo get_avatar( get_the_author_meta( 'user_email' ), 170, 'mystery', esc_attr( get_the_author() ) ); ?>
								</div>
								<div class="author-box-description">
									<h4><a class="author-link url fn" href="<?php echo esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ); ?>" rel="author" title="<?php printf( et_get_safe_localization( __( 'View all posts by %s', 'extra' ) ), get_the_author() ); ?>"><?php echo get_the_author(); ?></a></h4>
									<p class="note"><?php the_author_meta( 'description' ); ?></p>
									<ul class="social-icons">
										<?php foreach ( extra_get_author_contact_methods() as $method ) { ?>
											<li><a href="<?php echo esc_url( $method['url'] ); ?>" target="_blank"><span class="et-extra-icon et-extra-icon-<?php echo esc_attr( $method['slug'] ); ?> et-extra-icon-color-hover"></span></a></li>
										<?php } ?>
									</ul>
								</div>
							</div>
						</div>
						<?php } ?>

						<?php
						$related_posts = extra_get_post_related_posts();

						if ( $related_posts && extra_is_post_related_posts() ) {  ?>
						<div class="et_extra_other_module related-posts">
							<div class="related-posts-header">
								<h3><?php esc_html_e( 'Potrebbe interessarti anche', 'extra' ); ?></h3>
							</div>
							<div class="related-posts-content clearfix">
								<?php while ( $related_posts->have_posts() ) : $related_posts->the_post(); ?>
								<div class="related-post">
									<div class="featured-image"><?php
									echo et_extra_get_post_thumb( array(
										'size'                       => 'extra-image-small',
										'a_class'                    => array('post-thumbnail'),
										'post_format_thumb_fallback' => true,
										'img_after'                  => '<span class="et_pb_extra_overlay"></span>',
									));
									?></div>
									<h4 class="title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
									<!--<p class="date"><?php extra_the_post_date(); ?></p>-->
								</div>
								<?php endwhile; ?>
								<?php wp_reset_postdata(); ?>
							</div>
						</div>
						<?php } ?>
				<?php
					endwhile;
				else :
					?>
					<h2><?php esc_html_e( 'Post not found', 'extra' ); ?></h2>
					<?php
				endif;
				wp_reset_query();

				do_action( 'et_after_post' );
				?>

				<?php
                /*
				if ( ( comments_open() || get_comments_number() ) && 'on' == et_get_option( 'extra_show_postcomments', 'on' ) ) {
					comments_template( '', true );
				}
                */
				?>
			</div><!-- /.et_pb_extra_column.et_pb_extra_column_main -->

			<?php get_sidebar(); ?>

		</div> <!-- #content-area -->
	</div> <!-- .container -->
</div> <!-- #main-content -->

<?php get_footer();
