<?php


/* ********************************************************** */
/* CUSTOM DIVI BLOG MODULE */
/* ********************************************************** */

function fc_custom_blog_module() {

	if ( ! class_exists('ET_Builder_Module') )
		return;

	get_template_part( 'custom-modules/custom-blog-modules' );

  $cbm = new DS_Builder_Module_Blog();

	remove_shortcode( 'et_pb_blog' );

  add_shortcode( 'et_pb_blog', array($cbm, '_shortcode_callback') );

} // END function fc_custom_blog_module()

add_action( 'wp', 'fc_custom_blog_module', 9999 );


/* ********************************************************** */
/* CUSTOM DIVI BLOG SLIDER MODULE */
/* ********************************************************** */

function fc_custom_blog_slider_module() {

	if ( ! class_exists('ET_Builder_Module') )
		return;

	get_template_part( 'custom-modules/custom-articleslider-modules' );

  $cbm = new DS_Builder_Module_Blog();

	remove_shortcode( 'et_pb_post_slider' );

  add_shortcode( 'et_pb_post_slider', array($cbm, '_shortcode_callback') );

} // END function fc_custom_blog_module()

add_action( 'wp', 'fc_custom_blog_slider_module', 9999 );