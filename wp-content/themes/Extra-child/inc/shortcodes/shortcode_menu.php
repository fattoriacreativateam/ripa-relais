<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

if(!function_exists('getMenu_shortcode')){

	function getMenu_shortcode($atts) {
						
		$a = shortcode_atts(

			array(

				'id' => 11

	    ),

	    $atts

		);
		
		$menu = wp_nav_menu(
			array(
				'menu' => $a['id'],
				'echo' => false
			)
		);

		return $menu;
		
	}

	add_shortcode( 'getMenu', 'getMenu_shortcode' );

}

?>