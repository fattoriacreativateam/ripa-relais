<?php
/*Alert Message ACF is Required*/
function room_plugin_notice() {
    
    $screen = get_current_screen();
  
    if (($screen->post_type === 'rooms') && ( !class_exists('acf'))) {
        echo '<div class="notice notice-warning">';
        echo '<p>Custom Post Type ROOMS need <b>Advanced Custom Fields (Plugin)</b> to work properly!</p>';
        echo '</div>';
    }
    elseif (($screen->id === 'rooms') && ( $screen->base == 'post' )){ 
        $fields = get_fields();
        if (!empty($fields)){
            $currentFields = '';
            $currentImages = '';
            $comma = '';
            $i1=0;
            $i2=0;
            foreach($fields as $fieldsLabel => $value){
                $sep1 = ($i1==0)? '' : ' | ';
                $sep2 = ($i2==0)? '' : ' | ';
                if ((strpos($fieldsLabel, 'Image') !== false) || (strpos($fieldsLabel, 'gallery') !== false)){
                    $currentFields .= $sep1 . $fieldsLabel;
                    $i1++;
                }else{
                    $currentImages .= $sep2 . $fieldsLabel;
                    $i2++;
                }
            }
            echo '<div class="notice notice-info">';
            echo $currentFields;
            echo '<hr>';
            echo $currentImages;
            echo '</div>';
            
        }
    }
}                                     
add_action( 'admin_notices', 'room_plugin_notice' );


;?>