jQuery(document).on('ready', function() {
    jQuery( ".ui-select" ).selectmenu().selectmenu( "menuWidget" ).addClass( "overflow" );

    jQuery( ".ui-calendar" ).datepicker({
        dateFormat: 'dd/mm/yy',
        minDate: new Date(jQuery(this).data('start')),
        maxDate: new Date(jQuery(this).data('end'))
    });
    roomCustomButton();
    cookieBanner();
});


function roomCustomButton() {
    console.log('roomCustomButton');
    jQuery('#roomBlogView').find('.et_pb_post').each(function() {
        var link = "https://www.orangebooking.it/riparelais/",
            lang = jQuery('html').attr('lang'),
            button_label = (lang == "it-IT") ? "Prenota" : "Book",
            readmore_label = (lang == "it-IT") ? "Scopri di più" : "Discover more";
        jQuery(this).find('.post-content').append('<a class="button button-green button-booking" href="' + link + '" target="_BLANK">' + button_label + '</a>');
        jQuery(this).find('.post-content .more-link').text(readmore_label);
    });
}



function cookieBanner() {

    jQuery('body').fdCookieLaw({
        docs: "/wp-content/themes/Extra-child/libs/cookie-law/docs.complete.json",
        config: "/wp-content/themes/Extra-child/libs/cookie-law/config.json",
        debug: true
    });

    var cookie_wrapper = jQuery('#cookie_policy'),
        privacy_wrapper = jQuery('#privacy_policy');

    if (cookie_wrapper.length > 0) {
        cookie_wrapper.fdCookieLaw({
            page: "cookie",
            docs: "/wp-content/themes/Extra-child/libs/cookie-law/docs.complete.json",
            config: "/wp-content/themes/Extra-child/libs/cookie-law/config.json",
            bootstrap: false,
            banner: false,
            debug: true
        });
    }

    if (privacy_wrapper.length > 0) {
        privacy_wrapper.fdCookieLaw({
            page: "privacy",
            docs: "/wp-content/themes/Extra-child/libs/cookie-law/docs.complete.json",
            config: "/wp-content/themes/Extra-child/libs/cookie-law/config.json",
            bootstrap: false,
            banner: false,
            debug: true
        });
    }

}