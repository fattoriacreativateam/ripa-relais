<?php

/* CSS Style  - Register & Enqueue */
/* ========================================= */
function childTheme_styles() {



    wp_register_style('googleFonts', 'https://fonts.googleapis.com/css?family=Herr+Von+Muellerhoff', array(), true);
    wp_enqueue_style('googleFonts');

    wp_register_style('jQueryUI', get_stylesheet_directory_uri() . '/libs/jquery-ui/jquery-ui.min.css', array(), true);
    wp_enqueue_style('jQueryUI');

    wp_register_style( 'fdcCookieLaw', get_stylesheet_directory_uri() . '/libs/cookie-law/fdCookieLaw.min.css', array() );
    wp_enqueue_style( 'fdcCookieLaw' );

    wp_register_style( 'childThemeStyle', get_stylesheet_directory_uri() . '/css/child-theme.css', array() );
    wp_enqueue_style( 'childThemeStyle' );

}

add_action( 'wp_enqueue_scripts', 'childTheme_styles', 999 );

/* JS Style - Register & Enqueue  */
/* ========================================= */
function childTheme_scripts() {

    wp_register_script('jQueryUI',  get_stylesheet_directory_uri() . '/libs/jquery-ui/jquery-ui.min.js', array('jquery'),'1.4.5', true);
    wp_enqueue_script('jQueryUI');

    wp_register_script('FontAwesome',  'https://use.fontawesome.com/releases/v5.0.6/js/all.js', array('jquery'),'1.4.5', true);
    wp_enqueue_script('FontAwesome');

    wp_register_script('fdcCookieLaw',  get_stylesheet_directory_uri() . '/libs/cookie-law/jquery.fdCookieLaw.js', array('jquery'),'1.4.5', true);
    wp_enqueue_script('fdcCookieLaw');

    wp_register_script('childThemeJS',  get_stylesheet_directory_uri() . '/js/main-child-theme.js', array('jquery'),'1.1', true);
    wp_enqueue_script('childThemeJS');

}

add_action( 'wp_enqueue_scripts', 'childTheme_scripts' );



/* TRANSLATIONS */
/* =========================================
function my_child_theme_locale() {
    load_child_theme_textdomain( 'text_ripaRelais', get_stylesheet_directory() . '/languages' );
}

add_action( 'after_setup_theme', 'my_child_theme_locale' );


*/



/* ADD CUSTOM POST TYPE AND TAXONOMY */
/* =========================================
require_once('inc/rooms/room_post.php');
require_once('inc/rooms/room_group.php');
require_once('inc/rooms/room_fields.php');
require_once('inc/rooms/room_shortcode.php');
require_once('inc/rooms/room_message.php');

//Add Divi Builder tio custom Post Type
add_filter( 'et_builder_post_types', 'my_et_builder_post_types' );
function my_et_builder_post_types( $post_types ) {
    $post_types[] = 'rooms';

    return $post_types;
}

*/

//ADD Single Template from a Plug-In
/*add_filter('single_template', 'room_post_type_template');
* Filter the single_template with our custom function
* function room_post_type_template($single) {
*    global $wp_query, $post;
*    /* Checks for single template by post type
*    if ( $post->post_type == 'rooms' ) {
*        if ( file_exists( PLUGIN_PATH . '/single-rooms.php' ) ) {
*            return PLUGIN_PATH . '/single-rooms.php';
*        }
*    }
*    return $single;
}*/

/*
add_filter( 'pre_get_posts', 'my_get_posts' );
function my_get_posts( $query ) {

	if ( is_home() && $query->is_main_query() )
		$query->set( 'post_type', array( 'post', 'page', 'rooms' ) );

	return $query;
}

*/




/*
* Define a constant path to our single template folder
*/


load_theme_textdomain( 'riparelais', get_template_directory() . '/languages' );

/**
* Filter the single_template with our custom function
*/
add_filter('single_template', 'my_single_template');

/**
* Single template function which will choose our template
*/
function my_single_template($single) {
    global $wp_query, $post;

/**
* Checks for single template by category
* Check by category slug and ID
*/
    foreach((array)get_the_category() as $cat) :

        if(file_exists(get_stylesheet_directory() . '/single-cat-' . $cat->slug . '.php'))
        return get_stylesheet_directory() . '/single-cat-' . $cat->slug . '.php';

        elseif(file_exists(get_stylesheet_directory() . '/single-cat-' . $cat->term_id . '.php'))
        return get_stylesheet_directory() . '/single-cat-' . $cat->term_id . '.php';

        else return get_stylesheet_directory() . '/single-post.php';

    endforeach;
}


/* COOKIE AND PRIVACY SHORTCODE */
/* ========================================= */
function bookingForm_shortcode($atts) {

    $a = shortcode_atts( array(
        "day_start" => "",
        "day_end" => "",
        "rooms" => 1,
        "adults" => 2,
        "childs" => 0,
        "baby" => 0
    ), $atts );

    $markup = '';
	$markup .= '<form method="GET" class="module__custom-form booking-form" action="https://www.orangebooking.it/riparelais/" target="_blank">';

	$markup .= '<input type="hidden" name="IDCOMPANY" value="1" />';

	$markup .= '<div class="form-column">';
	$markup .= '<h4>'.__('Data<br>di arrivo').'</h4>';
	$markup .= '<input type="text" class="ui-calendar" name="IN" data-start="'.$a['day_start'].'" data-end="'.$a['day_end'].'" value="dd/mm/yyyy" />';
	$markup .= ' </div>';

	$markup .= '<div class="form-column">';
	$markup .= '<h4>'.__('Data di partenza').'</h4>';
	$markup .= '<input type="text" class="ui-calendar" name="OUT" data-start="'.$a['day_start'].'" data-end="'.$a['day_end'].'" value="dd/mm/yyyy" />';
	$markup .= '</div>';

	$markup .= '<div class="form-column">';
	$markup .= '<h4>'.__('Numero di camere').'</h4>';
	$markup .= '<select class="ui-select" name="ROOMS_NUM">';
	$markup .= '<option value="1">1</option>';
	$markup .= '<option value="2">2</option>';
	$markup .= '<option value="3">3</option>';
	$markup .= '<option value="4">4</option>';
	$markup .= '<option value="5">5</option>';
	$markup .= '</select>';
	$markup .= '</div>';

	$markup .= '<div class="form-column">';
	$markup .= '<h4>'.__('Adulti per camera').'</h4>';
	$markup .= '<select class="ui-select" name="R1_ADULT">';
	$markup .= '<option value="1">1</option>';
	$markup .= '<option value="2">2</option>';
	$markup .= '<option value="3">3</option>';
	$markup .= '<option value="4">4</option>';
	$markup .= '<option value="5">5</option>';
	$markup .= '<option value="6">6</option>';
	$markup .= '<option value="7">7</option>';
	$markup .= '<option value="8">8</option>';
	$markup .= '<option value="9">9</option>';
	$markup .= '</select>';
	$markup .= '</div>';

	$markup .= '<div class="form-column">';
	$markup .= '<h4>'.__('Bambini per camera').'</h4>';
	$markup .= '<select class="ui-select" name="R1_CHILD">';
	$markup .= '<option value="0">0</option>';
	$markup .= '<option value="1">1</option>';
	$markup .= '<option value="2">2</option>';
	$markup .= '<option value="3">3</option>';
	$markup .= '<option value="4">4</option>';
	$markup .= '<option value="5">5</option>';
	$markup .= '<option value="6">6</option>';
	$markup .= '<option value="7">7</option>';
	$markup .= '<option value="8">8</option>';
	$markup .= '<option value="9">9</option>';
	$markup .= '</select>';
	$markup .= '</div>';

	$markup .= '<div class="form-column">';
	$markup .= '<h4>'.__('Bebè per camera').'</h4>';
	$markup .= '<select class="ui-select" name="R1_INFANT">';
	$markup .= '<option value="0">0</option>';
	$markup .= '<option value="1">1</option>';
	$markup .= '<option value="2">2</option>';
	$markup .= '<option value="3">3</option>';
	$markup .= '<option value="4">4</option>';
	$markup .= '<option value="5">5</option>';
	$markup .= '<option value="6">6</option>';
	$markup .= '<option value="7">7</option>';
	$markup .= '<option value="8">8</option>';
	$markup .= '<option value="9">9</option>';
	$markup .= '</select>';
	$markup .= '</div>';

	$markup .= '<div class="form-column">';
	$markup .= '<button class="button" type="submit">'.__('Book').'</button>';
	$markup .= '</div>';

	$markup .= '</form>';

	$javascript = '<script>jQuery( ".ui-calendar_custom" ).datepicker( "option", "minDate", new Date(2007, 1 - 1, 1) );</script>';
    return $markup;

}

add_shortcode( 'bookingform', 'bookingForm_shortcode' );


// custom divi parts
include_once 'inc/custom_divi.php';

?>
