<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

/* CSS Style  - Register & Enqueue */
/* ========================================= */
function childTheme_styles() {

    wp_register_style('childThemeStyle', get_stylesheet_directory_uri() . '/css/child-theme.css', array('divi-style'), true);

    wp_enqueue_style('childThemeStyle');
}

add_action( 'wp_enqueue_scripts', 'childTheme_styles' );

/* JS Style - Register & Enqueue  */
/* ========================================= */
function childTheme_scripts() {
    
    wp_register_script('fdcCookieLaw',  get_stylesheet_directory_uri() . '/js/libs/jquery.fdCookieLaw.js', array('jquery'),'1.4.5', true);
    wp_enqueue_script('fdcCookieLaw');
    
    wp_register_script('childThemeJS',  get_stylesheet_directory_uri() . '/js/main-child-theme.js', array('jquery'),'1.1', true);

    wp_enqueue_script('childThemeJS');
    
}

add_action( 'wp_enqueue_scripts', 'childTheme_scripts' );

/* TRANSLATIONS */
/* ========================================= */
function my_child_theme_locale() {

    load_child_theme_textdomain( 'theme-child-domain', get_stylesheet_directory() . '/languages' );

}

add_action( 'after_setup_theme', 'my_child_theme_locale' );


/* INCLUDE SHORTCODES */
include_once 'inc/shortcodes/shortcode_policy.php'; // privacy policy

include_once 'inc/shortcodes/shortcode_menu.php';



/* ADD CUSTOM POST TYPE AND TAXONOMY */
/* ========================================= */
include_once('inc/rooms/room_post.php');
include_once('inc/rooms/room_group.php');
include_once('inc/rooms/room_fields.php');
include_once('inc/rooms/room_shortcode.php');
include_once('inc/rooms/room_message.php');


/* ADD CUSTOM DIVI BUILDER ADDON */
include_once('inc/DiviBuilder/functions.php');