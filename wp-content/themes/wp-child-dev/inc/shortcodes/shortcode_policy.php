<?php

/* COOKIE AND PRIVACY SHORTCODE */
/* ========================================= */
function policyPage_shortcode($atts) {
    $a = shortcode_atts( array(
        'page' => "cookie"
    ), $atts );
    
    
    switch ($a['page']) {
        case "cookie":
            $id = "cookie_policy";
            $class = "cookie-policy";
            break;
        case "privacy":
            $id = "privacy_policy";
            $class = "privacy-policy";
            break;
    }
    
    $markup = '<div id="'.$id.'" class="component__policy-page '.$class.'"></div>';

    return $markup;

}

add_shortcode( 'policyPage', 'policyPage_shortcode' );