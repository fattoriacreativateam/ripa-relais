<?php

/*ACF CUSTOM FIELDS*/
if(function_exists("register_field_group"))
{
	register_field_group(array (
		'id' => 'acf_rooms-fields',
		'title' => 'Rooms Fields',
		'fields' => array (
			array (
				'key' => 'field_59a3e4ad48b3b',
				'label' => 'Tipologia',
				'name' => '',
				'type' => 'tab',
			),
			array (
				'key' => 'field_5988776ed86e0',
				'label' => 'Tipologia',
				'name' => 'room_type',
				'type' => 'radio',
				'choices' => array (
					'Singola' => 'Singola',
					'Doppia' => 'Doppia',
					'Twin' => 'Twin',
					'Tripla' => 'Tripla',
					'Multipla / Family' => 'Multipla / Family',
					'Mini Appartamento' => 'Mini Appartamento',
				),
				'other_choice' => 0,
				'save_other_choice' => 0,
				'default_value' => '',
				'layout' => 'vertical',
			),
			array (
				'key' => 'field_5988857f30234',
				'label' => 'Metri Quadri',
				'name' => 'mq',
				'type' => 'number',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => 'MQ2',
				'min' => '',
				'max' => '',
				'step' => '',
			),
			array (
				'key' => 'field_59887817d86e1',
				'label' => 'Uso Singola',
				'name' => 'single_use_option',
				'type' => 'true_false',
				'conditional_logic' => array (
					'status' => 1,
					'rules' => array (
						array (
							'field' => 'field_5988776ed86e0',
							'operator' => '==',
							'value' => 'Doppia',
						),
					),
					'allorany' => 'all',
				),
				'message' => '',
				'default_value' => 0,
			),
			array (
				'key' => 'field_59887923e7a0d',
				'label' => 'Letto Aggiuntivo',
				'name' => 'extra_bed_1',
				'type' => 'true_false',
				'conditional_logic' => array (
					'status' => 1,
					'rules' => array (
						array (
							'field' => 'field_5988776ed86e0',
							'operator' => '==',
							'value' => 'Doppia',
						),
						array (
							'field' => 'field_5988776ed86e0',
							'operator' => '==',
							'value' => 'Tripla',
						),
					),
					'allorany' => 'any',
				),
				'message' => '',
				'default_value' => 0,
			),
			array (
				'key' => 'field_598dab81663e6',
				'label' => 'Lettino Aggiuntivo per Bambini',
				'name' => 'extra_bed_2',
				'type' => 'true_false',
				'conditional_logic' => array (
					'status' => 1,
					'rules' => array (
						array (
							'field' => 'field_5988776ed86e0',
							'operator' => '==',
							'value' => 'Doppia',
						),
						array (
							'field' => 'field_5988776ed86e0',
							'operator' => '==',
							'value' => 'Tripla',
						),
					),
					'allorany' => 'any',
				),
				'message' => '',
				'default_value' => 0,
			),
			array (
				'key' => 'field_59887e4c3d943',
				'label' => 'N° Posti Letto',
				'name' => 'persons_number',
				'type' => 'number',
				'conditional_logic' => array (
					'status' => 1,
					'rules' => array (
						array (
							'field' => 'field_5988776ed86e0',
							'operator' => '==',
							'value' => 'Multipla / Family',
						),
						array (
							'field' => 'field_5988776ed86e0',
							'operator' => '==',
							'value' => 'Mini Appartamento',
						),
					),
					'allorany' => 'any',
				),
				'default_value' => 3,
				'placeholder' => '',
				'prepend' => 'N°',
				'append' => '',
				'min' => '',
				'max' => 10,
				'step' => '',
			),
			array (
				'key' => 'field_59887f2db2460',
				'label' => 'Angolo Cottura',
				'name' => 'kitchen',
				'type' => 'true_false',
				'conditional_logic' => array (
					'status' => 1,
					'rules' => array (
						array (
							'field' => 'field_5988776ed86e0',
							'operator' => '==',
							'value' => 'Mini Appartamento',
						),
					),
					'allorany' => 'all',
				),
				'message' => '',
				'default_value' => 0,
			),
			array (
				'key' => 'field_59887512a2a80',
				'label' => 'Bagno',
				'name' => 'bathroom',
				'type' => 'true_false',
				'required' => 1,
				'message' => '',
				'default_value' => 1,
			),
			array (
				'key' => 'field_59887560a2a81',
				'label' => 'Accessori Bagno',
				'name' => 'bathroom_items',
				'type' => 'checkbox',
				'conditional_logic' => array (
					'status' => 1,
					'rules' => array (
						array (
							'field' => 'field_59887512a2a80',
							'operator' => '==',
							'value' => '1',
						),
					),
					'allorany' => 'any',
				),
				'choices' => array (
					'Doccia' => 'Doccia',
					'Vasca' => 'Vasca',
					'Vasca Idromassaggio' => 'Vasca Idromassaggio',
					'Vasca Idromassaggio doppia' => 'Vasca Idromassaggio doppia',
				),
				'default_value' => '',
				'layout' => 'horizontal',
			),
			array (
				'key' => 'field_59a3e44e1f757',
				'label' => 'Optionals',
				'name' => '',
				'type' => 'tab',
			),
			array (
				'key' => 'field_598883689624f',
				'label' => 'Optionals',
				'name' => 'room_optionals',
				'type' => 'checkbox',
				'choices' => array (
					'Facilitazioni per disabili' => 'Facilitazioni per disabili',
					'Non fumatori' => 'Non fumatori',
					'Animali ammessi' => 'Animali ammessi',
					'Aria condizionata' => 'Aria condizionata',
					'Riscaldamento indipendente' => 'Riscaldamento indipendente',
					'Frigobar' => 'Frigobar',
					'Telefono' => 'Telefono',
					'Cassaforte' => 'Cassaforte',
					'Balcone' => 'Balcone',
					'Asciugacapelli' => 'Asciugacapelli',
					'TV LCD' => 'TV LCD',
					'TV satellitare' => 'TV satellitare',
					'Internet WIFI' => 'Internet WIFI',
					'DVD Player' => 'DVD Player',
					'Culle disponibili' => 'Culle disponibili',
				),
				'default_value' => '',
				'layout' => 'vertical',
			),
			array (
				'key' => 'field_59a3e3855eed4',
				'label' => 'Gallery',
				'name' => '',
				'type' => 'tab',
			),
			array (
				'key' => 'field_598da6604e0f4',
				'label' => 'Gallery',
				'name' => 'gallery',
				'type' => 'true_false',
				'instructions' => 'Aggiungi immagini (max8) per la gallery',
				'message' => '',
				'default_value' => 0,
			),
			array (
				'key' => 'field_59887f9a2d2dd',
				'label' => 'Image 0',
				'name' => 'roomImages_0',
				'type' => 'image',
				'conditional_logic' => array (
					'status' => 1,
					'rules' => array (
						array (
							'field' => 'field_598da6604e0f4',
							'operator' => '==',
							'value' => '1',
						),
					),
					'allorany' => 'all',
				),
				'save_format' => 'object',
				'preview_size' => 'thumbnail',
				'library' => 'all',
			),
			array (
				'key' => 'field_59888065457db',
				'label' => 'Image 1',
				'name' => 'roomImages_1',
				'type' => 'image',
				'conditional_logic' => array (
					'status' => 1,
					'rules' => array (
						array (
							'field' => 'field_598da6604e0f4',
							'operator' => '==',
							'value' => '1',
						),
					),
					'allorany' => 'all',
				),
				'save_format' => 'object',
				'preview_size' => 'thumbnail',
				'library' => 'all',
			),
			array (
				'key' => 'field_5988813d457dc',
				'label' => 'Image 2',
				'name' => 'roomImages_2',
				'type' => 'image',
				'conditional_logic' => array (
					'status' => 1,
					'rules' => array (
						array (
							'field' => 'field_598da6604e0f4',
							'operator' => '==',
							'value' => '1',
						),
					),
					'allorany' => 'all',
				),
				'save_format' => 'object',
				'preview_size' => 'thumbnail',
				'library' => 'all',
			),
			array (
				'key' => 'field_598881e3953c4',
				'label' => 'Image 3',
				'name' => 'roomImages_3',
				'type' => 'image',
				'conditional_logic' => array (
					'status' => 1,
					'rules' => array (
						array (
							'field' => 'field_598da6604e0f4',
							'operator' => '==',
							'value' => '1',
						),
					),
					'allorany' => 'all',
				),
				'save_format' => 'object',
				'preview_size' => 'thumbnail',
				'library' => 'all',
			),
			array (
				'key' => 'field_5988824d56caf',
				'label' => 'Image 4',
				'name' => 'roomImages_4',
				'type' => 'image',
				'conditional_logic' => array (
					'status' => 1,
					'rules' => array (
						array (
							'field' => 'field_598da6604e0f4',
							'operator' => '==',
							'value' => '1',
						),
					),
					'allorany' => 'all',
				),
				'save_format' => 'object',
				'preview_size' => 'thumbnail',
				'library' => 'all',
			),
			array (
				'key' => 'field_5988825e716f4',
				'label' => 'Image 5',
				'name' => 'roomImages_5',
				'type' => 'image',
				'conditional_logic' => array (
					'status' => 1,
					'rules' => array (
						array (
							'field' => 'field_598da6604e0f4',
							'operator' => '==',
							'value' => '1',
						),
					),
					'allorany' => 'all',
				),
				'save_format' => 'object',
				'preview_size' => 'thumbnail',
				'library' => 'all',
			),
			array (
				'key' => 'field_59888277cfca3',
				'label' => 'Image 6',
				'name' => 'roomImages_6',
				'type' => 'image',
				'conditional_logic' => array (
					'status' => 1,
					'rules' => array (
						array (
							'field' => 'field_598da6604e0f4',
							'operator' => '==',
							'value' => '1',
						),
					),
					'allorany' => 'all',
				),
				'save_format' => 'object',
				'preview_size' => 'thumbnail',
				'library' => 'all',
			),
			array (
				'key' => 'field_59888286cfca5',
				'label' => 'Image 7',
				'name' => 'roomImages_7',
				'type' => 'image',
				'conditional_logic' => array (
					'status' => 1,
					'rules' => array (
						array (
							'field' => 'field_598da6604e0f4',
							'operator' => '==',
							'value' => '1',
						),
					),
					'allorany' => 'all',
				),
				'save_format' => 'object',
				'preview_size' => 'thumbnail',
				'library' => 'all',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'rooms',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'default',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));
}


?>