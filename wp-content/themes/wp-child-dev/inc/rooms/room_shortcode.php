<?php
/**
 * Returns a custom field value
 *
 * Usage [custom_field id="staff_position"]
 *
 */

function display_room_fields( $atts ) {
//Don't exe de shortcode error-return in admin panel
if ( ! is_admin() ) {
    $a = shortcode_atts( array(
                            'fields' => "all"
                            ), $atts );

    $fields = ($a['fields'] != 'all')? explode(',',$a['fields']) : $a['fields'] ;

    $post_fields = get_field_objects();
    echo '<pre>';
    var_dump ($post_fields);
    echo '</pre>';
    
    /*$iterator = new RecursiveIteratorIterator(new RecursiveArrayIterator($post_fields));
    foreach($iterator as $key => $value) {
     //if (($key === 'label') && ($value !== '' )) {
      echo '<pre>';      
        echo "$key => $value\n";
        echo '</pre>';
        //}
    }*/

    
    if( $post_fields ): ?>
    <ul>
        <li><?php echo $post_fields['room_type']['label'];?> : <?php echo $post_fields['room_type']['value'];?>  </li>
        <li><?php echo $post_fields['mq']['label'];?> : <?php echo $post_fields['mq']['value'];?>  </li>
        <li><?php echo $post_fields['single_use_option']['label'];?> : <?php echo $post_fields['single_use_option']['value'];?>  </li>
        <li><?php echo $post_fields['extra_bed_1']['label'];?> : <?php echo $post_fields['extra_bed_1']['value'];?>  </li>
        <li><?php echo $post_fields['extra_bed_2']['label'];?> : <?php echo $post_fields['extra_bed_2']['value'];?>  </li>
        <li><?php echo $post_fields['kitchen']['label'];?> : <?php echo $post_fields['kitchen']['value'];?>  </li>
        <li><?php echo $post_fields['persons_number']['label'];?> : <?php echo $post_fields['persons_number']['value'];?>  </li> 
        <li><?php echo $post_fields['bathroom']['label'];?> : <?php echo $post_fields['bathroom']['value'];?>  </li>
        <?php
        if(count($post_fields['bathroom_items']['value'])>0){
            ?>
            <li><?php echo $post_fields['bathroom_items']['label'];?></li>
            <ul>
                <?php
                foreach($post_fields['bathroom_items']['value'] as $bathroom_items){
                    ?>
                    <li><?php echo $bathroom_items;?></li>
                    <?php
                }
                ?>
            </ul>
            <?php
        }
        ?>
        <li><?php echo $post_fields['room_optionals']['label'];?></li>
        <ul>
            <li><?php echo $post_fields['room_optionals']['value'][0];?></li>
            <li><?php echo $post_fields['room_optionals']['value'][1];?></li>
            <li><?php echo $post_fields['room_optionals']['value'][2];?></li>
        </ul>       
        
    </ul>
    <ul>


                


    </ul>


    <?php endif;
    }
}
add_shortcode( 'room_fields', 'display_room_fields' );


function display_room_gallery( $atts ) {
//Don't exe de shortcode error-return in admin panel
$galleryStatus = $post_fields['gallery']['value'];
$gallery= array();
if ( (! is_admin()) && ($galleryStatus == true )   ) {
        
        $gallery[] = get_field('roomImages_0');
        $gallery[] = get_field('roomImages_1');
        $gallery[] = get_field('roomImages_2');
        $gallery[] = get_field('roomImages_3');
        $gallery[] = get_field('roomImages_4');
        $gallery[] = get_field('roomImages_5');
        $gallery[] = get_field('roomImages_6');
        $gallery[] = get_field('roomImages_7');
        
        if( !empty($gallery) ): ?>

	       <img src="<?php echo $image0['url']; ?>" alt="<?php echo $image0['alt']; ?>" />

        <?php endif;
    

    
    }
}
add_shortcode( 'room_gallery', 'display_room_gallery' );    











?>