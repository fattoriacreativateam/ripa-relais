<?php
if ( ! function_exists( 'room_group' ) ) {

// Register Custom Taxonomy
function room_group() {

	$labels = array(
		'name'                       => _x( 'Rooms Groups', 'Taxonomy General Name', 'ripaRelais' ),
		'singular_name'              => _x( 'Rooms Group', 'Taxonomy Singular Name', 'ripaRelais' ),
		'menu_name'                  => __( 'Rooms Groups', 'ripaRelais' ),
		'all_items'                  => __( 'All Groups', 'ripaRelais' ),
		'parent_item'                => __( 'Parent Group', 'ripaRelais' ),
		'parent_item_colon'          => __( 'Parent Group:', 'ripaRelais' ),
		'new_item_name'              => __( 'New Rooms Group', 'ripaRelais' ),
		'add_new_item'               => __( 'Add New Groups', 'ripaRelais' ),
		'edit_item'                  => __( 'Edit Group', 'ripaRelais' ),
		'update_item'                => __( 'Update Group', 'ripaRelais' ),
		'view_item'                  => __( 'View Group', 'ripaRelais' ),
		'separate_items_with_commas' => __( 'Separate Gropus with commas', 'ripaRelais' ),
		'add_or_remove_items'        => __( 'Add or remove Groups', 'ripaRelais' ),
		'choose_from_most_used'      => __( 'Choose from the most used', 'ripaRelais' ),
		'popular_items'              => __( 'Popular Groups', 'ripaRelais' ),
		'search_items'               => __( 'Search Group', 'ripaRelais' ),
		'not_found'                  => __( 'Not Found', 'ripaRelais' ),
		'no_terms'                   => __( 'No Groups', 'ripaRelais' ),
		'items_list'                 => __( 'Groups list', 'ripaRelais' ),
		'items_list_navigation'      => __( 'Groups list navigation', 'ripaRelais' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => false,
	);
	register_taxonomy( 'room_group', 'rooms', $args );

}
add_action( 'init', 'room_group', 0 );

}





?>