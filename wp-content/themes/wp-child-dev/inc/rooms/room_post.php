<?php

/* Room Custom Post */
/* ========================================= */
if ( ! function_exists('rooms_post_type') ) {
	
	// Register Custom Post Type
	function rooms_post_type() {
		$labels = array(
			'name'                  => _x( 'Rooms', 'Post Type General Name', 'ripaRelais' ),
			'singular_name'         => _x( 'Room', 'Post Type Singular Name', 'ripaRelais' ),
			'menu_name'             => __( 'Rooms', 'ripaRelais' ),
			'name_admin_bar'        => __( 'Rooms', 'ripaRelais' ),
			'archives'              => __( 'Rooms Archives', 'ripaRelais' ),
			'attributes'            => __( 'Room Attributes', 'ripaRelais' ),
			'parent_item_colon'     => __( 'Parent Room:', 'ripaRelais' ),
			'all_items'             => __( 'All Rooms', 'ripaRelais' ),
			'add_new_item'          => __( 'Add New Room', 'ripaRelais' ),
			'add_new'               => __( 'Add New Room', 'ripaRelais' ),
			'new_item'              => __( 'New Room', 'ripaRelais' ),
			'edit_item'             => __( 'Edit Room', 'ripaRelais' ),
			'update_item'           => __( 'Update Room', 'ripaRelais' ),
			'view_item'             => __( 'View Room', 'ripaRelais' ),
			'view_items'            => __( 'View Rooms', 'ripaRelais' ),
			'search_items'          => __( 'Search Room', 'ripaRelais' ),
			'not_found'             => __( 'Room Not found', 'ripaRelais' ),
			'not_found_in_trash'    => __( 'Room Not found in Trash', 'ripaRelais' ),
			'featured_image'        => __( 'Room Featured Image', 'ripaRelais' ),
			'set_featured_image'    => __( 'Set Room featured image', 'ripaRelais' ),
			'remove_featured_image' => __( 'Remove Room featured image', 'ripaRelais' ),
			'use_featured_image'    => __( 'Use as Room featured image', 'ripaRelais' ),
			'insert_into_item'      => __( 'Insert into Room', 'ripaRelais' ),
			'uploaded_to_this_item' => __( 'Uploaded to this Room', 'ripaRelais' ),
			'items_list'            => __( 'Rooms list', 'ripaRelais' ),
			'items_list_navigation' => __( 'Rooms list navigation', 'ripaRelais' ),
			'filter_items_list'     => __( 'Filter Rooms list', 'ripaRelais' ),
		);
		$rewrite = array(
			'slug'                  => 'camere',
			'with_front'            => true,
			'pages'                 => true,
			'feeds'                 => true,
		);
		$args = array(
			'label'                 => __( 'Room', 'ripaRelais' ),
			'description'           => __( 'Room data-sheet', 'ripaRelais' ),
			'labels'                => $labels,
			'supports'              => array( 'title', 'editor', 'excerpt', 'thumbnail', 'custom-fields', 'page-attributes'),
	    'taxonomies'            => array( 'room_group' , 'category' ),
			'hierarchical'          => false,
			'public'                => true,
			'show_ui'               => true,
			'show_in_menu'          => true,
			'menu_position'         => 10,
			'menu_icon'             => 'dashicons-admin-network',
			'show_in_admin_bar'     => true,
			'show_in_nav_menus'     => true,
			'can_export'            => true,
			'has_archive'           => true,		
			'exclude_from_search'   => false,
			'publicly_queryable'    => true,
			'rewrite'               => $rewrite,
			'capability_type'       => 'post'
		);
		register_post_type( 'rooms', $args );
	}

	add_action( 'init', 'rooms_post_type', 15 );

}


/*
 * Replace Custom Post Type slug with Taxonomy Term slug in url
 * Version: 1.1
 */
/*function room_post_type_link( $post_link, $id = 0 ){
    
    $taxonomy_exist = taxonomy_exists('room_group');
    //returns false if global $wp_taxonomies['my_taxonomy'] is not set

    if ($taxonomy_exist){ 
        $post = get_post( $id );

        $post_type = 'rooms';
        
        $hook = 'camere';

        $taxonomy = 'room_group';

        if( !is_object( $post ) || $post->post_type != $post_type )
            return $post_link;

        $slug = 'camere';	// Slug per negozi che non hanno nessuna categoria negozio impostata

        if( $terms = wp_get_object_terms( $post->ID, $taxonomy ) )
            $slug = $terms[0]->slug;

        return str_replace( '%'. $hook .'%', $slug, $post_link );
    }else{
        return $post_link;
    }
}

add_filter( 'post_type_link', 'room_post_type_link', 1, 3 );*/








?>