<?php

function ex_divi_child_theme_setup() {

  if ( class_exists('ET_Builder_Module')) {

    // this is where your new module class will go

    class ET_Builder_Module_Custom_Post_Slider extends ET_Builder_Module {

			function init() {

				$this->name = esc_html__( 'Custom Posts Slider', 'et_builder' );
				$this->slug = 'et_pb_custom_posts_slider';

				$this->whitelisted_fields = array(
					'posts_number'
				);

				$this->fields_defaults = array(
					'posts_number' => array( '1' ),
				);

				$this->options_toggles = array(
					'general' => array(
						'toggles' => array(
							'main_content' => esc_html__( 'Content', 'et_builder' ),
							//'elements' => esc_html__( 'Elements', 'et_builder' ),
							//'featured_image' => esc_html__( 'Featured Image', 'et_builder' ),
							//'background' => esc_html__( 'Background', 'et_builder' ),
						),
					)
				);				
				
			} // end function init() 

			function get_fields() {
				$fields = array(
					'posts_number' => array(
						'label'             => esc_html__( 'Posts Number', 'et_builder' ),
						'type'              => 'text',
						'option_category'   => 'configuration',
						'description'       => esc_html__( 'Choose how many posts you would like to display in the slider.', 'et_builder' ),
						'toggle_slug'       => 'main_content',
						'computed_affects'  => array(
							'__posts',
						),
					),
				);

				return $fields;

			} // end function get_fields() 

		} // END class ET_Builder_Module_Custom_Post_Slider extends ET_Builder_Module

  	$et_builder_module_custom_posts = new ET_Builder_Module_Custom_Post_Slider;

  	add_shortcode( 'et_pb_custom_posts', array($et_builder_module_custom_posts, '_shortcode_callback') );


  	class ET_Builder_Module_Custom_Custom_Custom extends ET_Builder_Module {

			function init() {

				$this->name       = esc_html__( 'Custom Custom Custom', 'et_builder' );
				$this->slug       = 'et_pb_custom_custom_custom';
				
			} // end function init() 

		} // END class ET_Builder_Module_Custom_Custom_Custom extends ET_Builder_Module

  	$et_builder_module_custom_custom_custom = new ET_Builder_Module_Custom_Custom_Custom;

  	add_shortcode( 'et_pb_custom_custom_custom', array($et_builder_module_custom_custom_custom, '_shortcode_callback') );


  	/* ADD RENDER TAXONOMIES ROOMS 
  	if ( ! function_exists( 'et_builder_include_custom_taxonomy_option' ) ) :
			function et_builder_include_custom_taxonomy_option( $args = array() ) {

				$defaults = apply_filters( 'et_builder_include_custom_taxonomy_defaults', array (
					'use_terms' => true,
					'term_name' => 'room_group',
				) );

				$args = wp_parse_args( $args, $defaults );

				$output = "\t" . "<% var et_pb_include_custom_taxonomy_temp = typeof et_pb_include_taxonomies !== 'undefined' ? et_pb_include_taxonomies.split( ',' ) : []; %>" . "\n";

				if ( $args['use_terms'] ) {
					$cats_array = get_terms( $args['term_name'] );
				} else {
					$cats_array = get_categories( apply_filters( 'et_builder_get_taxonomies_args', 'hide_empty=0' ) );
				}

				if ( empty( $cats_array ) ) {
					$output = '<p>' . esc_html__( "You currently don't have any Room assigned to a taxonomy.", 'et_builder' ) . '</p>';
				}

				foreach ( $cats_array as $category ) {
					$contains = sprintf(
						'<%%= _.contains( et_pb_include_custom_taxonomy_temp, "%1$s" ) ? checked="checked" : "" %%>',
						esc_html( $category->term_id )
					);

					$output .= sprintf(
						'%4$s<label><input type="checkbox" name="et_pb_include_taxonomies" value="%1$s"%3$s> %2$s</label><br/>',
						esc_attr( $category->term_id ),
						esc_html( $category->name ),
						$contains,
						"\n\t\t\t\t\t"
					);
				}

				$output = '<div id="et_pb_include_taxonomies">' . $output . '</div>';

				return apply_filters( 'et_builder_include_custom_taxonomy_option', $output );
			}
		endif;
		*/

  } // END if ( class_exists('ET_Builder_Module')) 

} // END function ex_divi_child_theme_setup()

add_action('et_builder_ready', 'ex_divi_child_theme_setup');


