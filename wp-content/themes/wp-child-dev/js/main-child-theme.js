jQuery(document).on('ready', function() {
    cookieBanner();
});

function easyHeightEqualizer(element,assign) {
    
    element = (element === undefined || element === "") ? ".HEqualizer" : element;
    assign = (element === false || element === true) ? element : assign;
    assign = (assign === undefined || assign === "") ? true : assign;
    
    var maxHeight = 0,
        elementHeight = 0;
        
    jQuery(element).each(function(){
        elementHeight = jQuery(this).height();
        if (elementHeight > maxHeight) { maxHeight = elementHeight }
    });
    
    if (assign === true) {
        jQuery(element).height(maxHeight);
    }else{
        return maxHeight;
    }
}

function cookieBanner() {
    
    jQuery('body').fdCookieLaw({
        docs: "/wp-content/themes/Divi-Child/fdc-cookielaw/docs.complete.json",
        config: "/wp-content/themes/Divi-Child/fdc-cookielaw/config.json",
        debug: true
    });
    
    var cookie_wrapper = jQuery('#cookie_policy'),
        privacy_wrapper = jQuery('#privacy_policy');
    
    if (cookie_wrapper.length > 0) {
        cookie_wrapper.fdCookieLaw({
            page: "cookie",
            docs: "/wp-content/themes/Divi-Child/fdc-cookielaw/docs.complete.json",
            config: "/wp-content/themes/Divi-Child/fdc-cookielaw/config.json",
            bootstrap: false,
            banner: false,
            debug: true
        });
    }
    
    if (privacy_wrapper.length > 0) {
        privacy_wrapper.fdCookieLaw({
            page: "privacy",
            docs: "/wp-content/themes/Divi-Child/fdc-cookielaw/docs.complete.json",
            config: "/wp-content/themes/Divi-Child/fdc-cookielaw/config.json",
            bootstrap: false,
            banner: false,
            debug: true
        });
    }
    
}


function remove_style(all) {
  var i = all.length;
  var j, is_hidden;

  // Presentational attributes.
  var attr = [
    'align',
    'background',
    'bgcolor',
    'border',
    'cellpadding',
    'cellspacing',
    'color',
    'face',
    'height',
    'hspace',
    'marginheight',
    'marginwidth',
    'noshade',
    'nowrap',
    'valign',
    'vspace',
    'width',
    'vlink',
    'alink',
    'text',
    'link',
    'frame',
    'frameborder',
    'clear',
    'scrolling',
    'style'
  ];

  var attr_len = attr.length;

  while (i--) {
    is_hidden = (all[i].style.display === 'none');

    j = attr_len;

    while (j--) {
      all[i].removeAttribute(attr[j]);
    }

    // Re-hide display:none elements,
    // so they can be toggled via JS.
    if (is_hidden) {
      all[i].style.display = 'none';
      is_hidden = false;
    }
  }
}