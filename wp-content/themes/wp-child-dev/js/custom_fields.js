jQuery(document).ready(function(){

	activateUnpublishDate(); // Manage custom_field_unpublish_date in functions.php for admin posts

});


/* Class function for unpublish date */
function activateUnpublishDate(){

	console.log("activateUnpublishDate");

	var thisClass = this;

	this.btnEdit = jQuery('a.edit-timestampend');
	this.btnEdit.click(function(){thisClass.edit();});

	this.btnCancel = jQuery('a.cancel-timestampend');
	this.btnCancel.click(function(){thisClass.cancel();});

	this.btnSave = jQuery('a.save-timestampend');
	this.btnSave.click(function(){thisClass.save();});

	this.btnClean = jQuery('a.clean-timestampend');
	this.btnClean.click(function(){thisClass.clean();});

	this.unpublish_date = jQuery('#unpublish_date');

	this.unpublish_date_view = jQuery('span#unpublish_date_view');

	
	this.formDate = {
		jj: jQuery('#unpublish_jj'),
		mm: jQuery('#unpublish_mm'),
		aa: jQuery('#unpublish_aa'),
		hh: jQuery('#unpublish_hh'),
		mn: jQuery('#unpublish_mn')
	}		

	this.edit = function(){

		this.btnEdit.hide();

		jQuery('#timestampenddiv').slideToggle();

		if(this.unpublish_date.val()){

		}

	}; // END this.edit = function()



	this.cancel = function(){

		this.btnEdit.show();

		jQuery('#timestampenddiv').slideToggle();

	}; // END this.cancel = function()



	this.save = function(){

		console.log(this.formDate);

		// CHECK CORRECT DATA
		var jj = this.formDate.jj.val().trim();
		var mm = this.formDate.mm.val();
		var aa = this.formDate.aa.val().trim();
		var hh = this.formDate.hh.val().trim();
		var mn = this.formDate.mn.val().trim();

		console.log("\tjj: " + jj);
		console.log("\tmm: " + mm);
		console.log("\taa: " + aa);
		console.log("\thh: " + hh);
		console.log("\tmn: " + mn);

		if(!jj || !mm || !aa || !hh || !mn)
			return false;

		if(jj.length<2)
			jj = "0" + jj;

		if(aa.length<4)
			return false;

		if(hh.length<2)
			hh = "0" + hh;

		if(mn.length<2)
			mn = "0" + mn;

		var unpublishDate = aa + "-" + mm + "-" + jj + " " + hh + ":" + mn;

		console.log("unpublishDate: " + unpublishDate);

		this.unpublish_date.val(unpublishDate);

		var unpublishDateView = jj + " " + jQuery('#unpubish_mm :selected').text() + ", " + aa + " @ " + hh + ":" + mn;

		this.unpublish_date_view.empty().append(unpublishDateView);

		this.btnClean.show();

		this.cancel();

		return true;

	}; // END this.save = function()


	this.clean = function(){

		if(!confirm('Confirm?'))
			return false;

		this.btnClean.hide();

		this.unpublish_date_view.empty().append( this.unpublish_date_view.attr('data-default') );

		jQuery.each(thisClass.formDate, function(v, field){

			if(v!='mm'){

				field.val('');

			} else {

				field.eq(0).prop('selected', true);

			}

		});

		this.unpublish_date.val('');

		this.cancel();

	}; // END this.clean = function()


}; // END Class Function activateUnpublishDate()