# Wordpress Theme Child

Official Super Sloth Team Wordpress Child Theme

## IMPORTANT
Remember, use config.json to configure grunt task and child theme
The "themename" variable in config.json must contain only the Parent Theme Name, the suffix "-child" will be added automatically

## GETTING START
1. npm install
2. grunt install