module.exports = function (grunt) {

    grunt.initConfig({

        pkg: grunt.file.readJSON('package.json'),

        sass: {
            dist: {
                options: {
                    style: 'expanded'
                },
                files: {
                    //'<%= pkg.params.src %><%= pkg.params.css %>': '<%= pkg.params.src %><%= pkg.params.sass %>'
                    'wp-content/themes/Extra-child/css/child-theme.css' : 'wp-content/themes/Extra-child/sass/boilerSass.scss'
                }
            }
        },
        watch: {
            files: ['wp-content/themes/Extra-child/sass/**/*.scss'],
            tasks: ['sass_compile']
        }
    });

    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-watch');

    // Developing Tasks
    grunt.registerTask('sass_compile', ['sass']);
    grunt.registerTask('default', ['sass','watch']);
    grunt.registerTask('dev', ['sass','watch']);

};